/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jivkosabev.stringcalculator.test;

import jivkosabev.stringcalculator.StringCalculator;
import org.junit.jupiter.api.Assertions;

import org.junit.jupiter.api.Test;

/**
 *
 * @author jsabev
 */
public class CalculatorTest {

    /**
     * Test the empty string
     */
    @Test
    public void testBaseCase() {
        String input = "";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 0);
    }

    @Test
    public void testDefaultDelimiter() {
        String input = "1,2,3,4,5";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 15);
    }

    @Test
    public void testNewLineCharacters() {
        String input = "1,\n2 ,3\n,4\n,5\n";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 15);
    }

    @Test
    public void testCustomDelimiter() {
        String input = "//;\n1;3;4";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 8);

        input = "//$\n1$2$3";
        sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 6);

        input = "//@\n2@3@8";
        sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 13);

        //special characters as delimiters
        input = "//|\n2|3|8";
        sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 13);

    }
    
    @Test
    public void testNegativeNumbers(){
        String input = "1,-1,-2";
        Assertions.assertThrows(RuntimeException.class, () -> {StringCalculator.add(input);});
    }

    @Test
    public void testMaxNumbers() {
        String input = "1,2000,1";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 2);
    }

    @Test
    public void testArbitraryDelimiterLength() {
        String input = "//@@@\n1@@@2@@@3";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 6);
    }

    @Test
    public void testMultipleDelimiters() {
        String input = "//@,$\n1$2@3";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 6);
    }

    @Test
    public void testMultipleDelimitersArbitraryLength() {
        String input = "//@#,$#\n1@#2$#3";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 6);
    }
    
    @Test
    public void testSpaceAsDelimiter(){
        String input = "// \n1 2 3";
        int sum = StringCalculator.add(input);
        Assertions.assertTrue(sum == 6);
    }
    
    

}
