/**
 * @author Jivko Sabev (jivko.sabev@gmail.com)
*/
package jivkosabev.stringcalculator;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 *
 * @author jsabev (jivko.sabev@gmail.com)
 *
 */
public class StringCalculator {

    private static final String DELIMITER_REGEX = "^//([\\S\\s]+)\n{1}";

    /**
     * Add the strings in number as per instructions provided in email
     *
     * @param number
     * @return the sum of the numbers
     */
    public static int add(String number) {
        //remove control character from beginning (if any)
        String input = number.replaceAll(DELIMITER_REGEX, "");
        final int maxValue = 1000; //ignore tokens over maxValue

        //split the number into tokens based on delimiters
        String[] tokens = input.split(getDelimiters(number));

        //Obtain the numbers to sum into an array list of integers
        List<Integer> toSum = Arrays.asList(tokens).stream()
                .filter(s -> (s.trim().length() > 0)) //filter out empty strings or all blanks
                .map(n -> Integer.parseInt(n.trim())) //convert to integer
                .filter(n -> n <= maxValue) //filter out values over maxValue
                .collect(Collectors.toList()); //obtain the list to sum

        //if any negative values complain to user        
        List<Integer> negativeValues = toSum.stream()
                .filter(n -> (n < 0))
                .collect(Collectors.toList());
        if (!negativeValues.isEmpty()) {
            
            //concatenate the negative numbers to string
            String negatives = negativeValues.stream()
                    .map(n -> n + " ").reduce("", String::concat);
            throw new RuntimeException("Negatives not allowed: " + negatives);
        }
        
        //return toSum.stream().mapToInt(m -> m).summaryStatistics().getSum();
        //return toSum.stream().reduce(0, Integer::sum);
        return toSum.stream().collect(Collectors.summingInt(Integer::intValue));

    }

    /**
     * Get a regular expression to be used to split the input into tokens. The
     * regex is obtained from the delimiter control character at the beginning of
     * the number 
     * @param number
     * @return A regular expression that can be used to split the input into
     * tokens
     */
    private static String getDelimiters(String number) {
        Pattern delimiterRegex = Pattern.compile(DELIMITER_REGEX);
        Matcher matcher = delimiterRegex.matcher(number);
        if (matcher.find()) {
            String[] multipleDelimiters = matcher.group(1).split(","); //assuming multiple delimiters are separated by ",";
            String delmiter = "";
            for (String token : multipleDelimiters) {
                delmiter = delmiter + Pattern.quote(token) + "|";
            }
            return delmiter.substring(0, delmiter.length() - 1); //remove the last | 
        }
        return ","; //defult delimiter

    }

    public static void main(String[] args) {
        //test base case - empty string
        int sum = add("");
        assert (sum == 0);
        System.out.println(sum);
        //test default delimiter ,
        String input = "1,2,5";
        sum = add(input);
        assert (sum == 8);
        System.out.println(sum);
        //test new line (\n) characters in the input
        input = "1, 2\n, 3\n,4";
        sum = add(input);
        assert (sum == 10);
        System.out.println(sum);
        //test custom delimiters
        input = "//;\n1;3;4";
        sum = add(input);
        assert (sum == 8);
        System.out.println(sum);    
        //test custom delimiter with new line in iput
        input = "//;\n1;3\n;\n4";
        sum = add(input);
        assert (sum == 8);
        System.out.println(sum);
        //test custom delimiter with special characters (| is a regex)
        input = "//|\n1|3|4";
        sum = add(input);
        assert (sum == 8);
        System.out.println(sum);
    }
}
